package controllers

import (
	"log"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
	"github.com/jonathanjojo/level-api/models"
	"github.com/jonathanjojo/level-api/services"
)

type LevelController struct {
	LevelService services.LevelService
}

// GetLevel: retrieve a single level by id
func (c *LevelController) GetLevel(ctx *gin.Context) {
	var level models.LevelModel

	id, _ := strconv.ParseInt(ctx.Param("id"), 10, 64)
	level = c.LevelService.GetLevel(id)

	ctx.JSON(http.StatusOK, level)
}

// GetAllLevel: retrieve all levels, accept is_assigned, level, and name query parameter
func (c *LevelController) GetAllLevel(ctx *gin.Context) {
	isAssignedParam := ctx.DefaultQuery("is_assigned", "")
	levelParam := ctx.DefaultQuery("level", "")
	nameParam := ctx.DefaultQuery("name", "")

	filters := services.LevelFilter{
		IsAssigned: isAssignedParam,
		Level:      levelParam,
		Name:       nameParam,
	}

	levels := c.LevelService.GetAllLevel(filters)

	ctx.JSON(http.StatusOK, levels)
}

// CreateLevel: create a new level
func (c *LevelController) CreateLevel(ctx *gin.Context) {
	var newLevel models.LevelModel
	err := ctx.BindJSON(&newLevel)
	if err != nil {
		log.Println(err)
	}
	newLevel = c.LevelService.CreateLevel(newLevel)

	ctx.JSON(http.StatusOK, newLevel)
}

// UpdateLevel: modify a level by id
func (c *LevelController) UpdateLevel(ctx *gin.Context) {
	var updatedLevel models.LevelModel

	id, _ := strconv.ParseInt(ctx.Param("id"), 10, 64)
	err := ctx.BindJSON(&updatedLevel)
	if err != nil {
		log.Println(err)
	}
	updatedLevel = c.LevelService.UpdateLevel(id, updatedLevel)

	ctx.JSON(http.StatusOK, updatedLevel)
}

// DeleteLevel: soft delete a level by id, set is_deleted to true
func (c *LevelController) DeleteLevel(ctx *gin.Context) {
	var level models.LevelModel

	id, _ := strconv.ParseInt(ctx.Param("id"), 10, 64)
	level = c.LevelService.DeleteLevel(id)

	ctx.JSON(http.StatusOK, level)
}
