package main

import (
	"log"
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/jonathanjojo/level-api/config"
	"github.com/jonathanjojo/level-api/controllers"
	"github.com/jonathanjojo/level-api/services"
)

func main() {
	db := config.InitDB()

	router := gin.Default()

	levelService := services.LevelService{DB: db}
	levelController := controllers.LevelController{LevelService: levelService}
	levelEndpoints := router.Group("/level")
	{
		levelEndpoints.GET("/:id", levelController.GetLevel)
		levelEndpoints.GET("/", levelController.GetAllLevel)
		levelEndpoints.POST("/", levelController.CreateLevel)
		levelEndpoints.PATCH("/:id", levelController.UpdateLevel)
		levelEndpoints.DELETE("/:id", levelController.DeleteLevel)
	}

	log.Fatal(http.ListenAndServe(":8080", router))
}
