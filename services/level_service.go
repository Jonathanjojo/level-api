package services

import (
	"fmt"

	"github.com/jinzhu/gorm"
	"github.com/jonathanjojo/level-api/models"
)

type LevelService struct {
	DB *gorm.DB
}

type LevelFilter struct {
	IsAssigned string
	Level      string
	Name       string
}

// GetLevel: retrieve a single level from database by id
func (s *LevelService) GetLevel(idParam int64) models.LevelModel {
	var level models.LevelModel
	s.DB.Where("is_deleted = ? AND id = ?", false, idParam).First(&level)
	return level
}

// GetAllLevel: retrieve all levels based on passed filters
func (s *LevelService) GetAllLevel(filters LevelFilter) []models.LevelModel {
	var levels []models.LevelModel
	filteredLevels := s.DB.Where("is_deleted = ?", false).Find(&levels)

	if filters.IsAssigned != "" {
		IsAssigned := filters.IsAssigned == "true"
		filteredLevels = filteredLevels.Where("is_assigned = ?", IsAssigned).Find(&levels)
	}
	if filters.Level != "" {
		filteredLevels = filteredLevels.Where("level = ?", filters.Level).Find(&levels)
	}
	if filters.Name != "" {
		filteredLevels = filteredLevels.Where("name = ?", filters.Name).Find(&levels)
	}

	return levels
}

// CreateLevel: insert a new level
func (s *LevelService) CreateLevel(level models.LevelModel) models.LevelModel {
	s.DB.Create(&level)
	return level
}

// UpdateLevel: modify a level based on id
func (s *LevelService) UpdateLevel(idParam int64, level models.LevelModel) models.LevelModel {
	var levelFromDb models.LevelModel
	s.DB.Where("is_deleted = ? AND id = ?", false, idParam).First(&levelFromDb)
	s.DB.Model(&levelFromDb).Updates(level)
	return levelFromDb
}

// DeleteLevel: set a level to is_deleted by id without modifying the updated_at field
func (s *LevelService) DeleteLevel(idParam int64) models.LevelModel {
	var levelFromDb models.LevelModel
	s.DB.Where("is_deleted = ? AND id = ?", false, idParam).First(&levelFromDb)
	fmt.Println(levelFromDb)
	s.DB.Model(&levelFromDb).UpdateColumn("is_deleted", true)
	return levelFromDb
}
