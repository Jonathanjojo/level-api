package models

import "time"

// BaseModel: reusable base implementation of a model
type BaseModel struct {
	ID        int64     `gorm:"PRIMARY_KEY;AUTO_INCREMENT;NOT_NULL" json:"id"`
	CreatedAt time.Time `gorm:"NOT_NULL" json:"created_at"`
	UpdatedAt time.Time `gorm:"NOT_NULL" json:"updated_at"`
	IsDeleted bool      `gorm:"default:false;NOT_NULL" json:"is_deleted"`
}
