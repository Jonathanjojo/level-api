package models

import "github.com/jinzhu/gorm"

// LevelModel: model representation of a level object
type LevelModel struct {
	BaseModel
	Name          string `gorm:"NOT_NULL" sql:"COLLATE utf8mb4_unicode_ci" json:"name"`
	Level         uint   `gorm:"NOT_NULL" json:"level"`
	IsAssigned    bool   `gorm:"default:false;NOT_NULL" json:"is_assigned"`
	LevelConfigId int64  `json:"level_config_id"`
	BallConfigId  int64  `json:"ball_config_id"`
}

// AfterCreate: custom hook to update the level and ball config ID with ID
func (level *LevelModel) AfterCreate(scope *gorm.Scope) (err error) {
	scope.DB().Model(level).Update("level_config_id", level.ID)
	scope.DB().Model(level).Update("ball_config_id", level.ID)
	return
}
