package config

import (
	"fmt"
	"os"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
	"github.com/jonathanjojo/level-api/models"
)

type dbConfiguration struct {
	DBHost     string
	DBUser     string
	DBPassword string
	DBName     string
}

// InitDB: initialize and return db connection
func InitDB() *gorm.DB {
	params := dbConfiguration{
		DBHost:     os.Getenv("DB_HOST"),
		DBUser:     os.Getenv("DB_USER"),
		DBPassword: os.Getenv("DB_PASSWORD"),
		DBName:     os.Getenv("DB_NAME"),
	}

	db, err := gorm.Open(
		"mysql",
		fmt.Sprintf("%s:%s@(%s)/%s?loc=UTC&charset=utf8mb4,utf8&parseTime=True",
			params.DBUser,
			params.DBPassword,
			params.DBHost,
			params.DBName,
		),
	)

	if err != nil {
		panic(err)
	}

	db.LogMode(true)
	db.AutoMigrate(&models.LevelModel{})
	return db
}
